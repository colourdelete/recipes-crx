function getIngrsOnEnter(e) {
  if (e.key === 'Enter') {
    getIngrs();
  }
}

document.getElementById('url').addEventListener('keypress', getIngrsOnEnter);
document.getElementById('server-url').addEventListener('keypress', getIngrsOnEnter);
document.getElementById('instacart-store-id').addEventListener('keypress', getIngrsOnEnter);

let infoIcon =
  '<svg style="width:24px;height:24px" viewBox="0 0 24 24">\n' +
  '    <path fill="currentColor" d="M11,9H13V7H11M12,20C7.59,20 4,16.41 4,12C4,7.59 7.59,4 12,4C16.41,4 20,7.59 20,12C20,16.41 16.41,20 12,20M12,2A10,10 0 0,0 2,12A10,10 0 0,0 12,22A10,10 0 0,0 22,12A10,10 0 0,0 12,2M11,17H13V11H11V17Z" />\n' +
  '</svg>';

let checkIcon =
  '<svg style="width:24px;height:24px" viewBox="0 0 24 24">\n' +
  '    <path fill="currentColor" d="M21,7L9,19L3.5,13.5L4.91,12.09L9,16.17L19.59,5.59L21,7Z" />\n' +
  '</svg>';

let alertIcon =
  '<svg style="width:24px;height:24px" viewBox="0 0 24 24">\n' +
  '    <path fill="currentColor" d="M11,15H13V17H11V15M11,7H13V13H11V7M12,2C6.47,2 2,6.5 2,12A10,10 0 0,0 12,22A10,10 0 0,0 22,12A10,10 0 0,0 12,2M12,20A8,8 0 0,1 4,12A8,8 0 0,1 12,4A8,8 0 0,1 20,12A8,8 0 0,1 12,20Z" />\n' +
  '</svg>';

function getIngrs() {
  let instacartStoreID = document.getElementById('instacart-store-id').value;
  if (instacartStoreID === '') {
    instacartStoreID = document.getElementById('instacart-store-id').placeholder;
  }
  let serverURL = document.getElementById('server-url').value;
  if (serverURL === '') {
    serverURL = document.getElementById('server-url').placeholder;
  }
  let URL = document.getElementById('url').value;
  if (URL === '') {
    URL = document.getElementById('url').placeholder;
  }
  console.log('unencoded url', URL);
  let encodedURL = encodeURIComponent(URL);
  console.log('encoded url', encodedURL);
  fetch(serverURL + '/api/ingr/' + encodedURL)
    .then(function (response) {
      console.log('server responded with code', response.status, response.statusText)
      if (response.status !== 200) {
        document.getElementById('result-icon').innerHTML = alertIcon;
        document.getElementById('result').innerHTML
          = response.status.toString() + ' ' + response.statusText + '\n'
          + '\nCheck your settings to make sure they are valid.';
      }
      else {
        response.json().then(function (data) {
          let fmted = '';
          if (data['ingrs'].length === 0) {
            document.getElementById('result-icon').innerHTML = alertIcon;
            document.getElementById('result-content').innerText = 'No ingredients found.';
          } else {
            fmted = 'Ingredients:<br><ul>';
            for (let i = 0; i < data['ingrs'].length; i ++) {
              let amountFmted = '';
              if (data['ingrs'][i]['measure']['amount'] !== 0) {
                amountFmted
                  = data['ingrs'][i]['measure']['amount'] + ' '
                  + data['ingrs'][i]['measure']['name'] + '\n';
              } else if (data['ingrs'][i]['measure']['weight'] !== 0) {
                amountFmted
                  = data['ingrs'][i]['measure']['weight'] + ' '
                  + data['ingrs'][i]['measure']['name'] + '\n';
              }
              let instacartURL = 'https://www.instacart.ca/store/' + instacartStoreID + '/search_v3/' + encodeURIComponent(data['ingrs'][i]['name']);
              let googleURL = 'https://www.google.com/search?q=' + encodeURIComponent(data['ingrs'][i]['name']);
              let googleImageURL = 'https://www.google.com/search?tbm=isch&q=' + encodeURIComponent(data['ingrs'][i]['name']);
              fmted
                += '<li>' + data['ingrs'][i]['name'] + ' ' + amountFmted
                + '<br />Find on <a class="w3-hover-text-light-blue" target="_blank" href="' + instacartURL + '">Instacart</a> | '
                + '<a class="w3-hover-text-light-blue" target="_blank" href="' + googleURL + '">Google</a> | '
                + '<a class="w3-hover-text-light-blue" target="_blank" href="' + googleImageURL + '">Google Images</a></li>';
            }
            fmted += '</ul>';
            document.getElementById('result-icon').innerHTML = checkIcon;
            document.getElementById('result-content').innerHTML = fmted;
          }
        })
      }
    })
    .catch(function (error) {
      document.getElementById('result-icon').innerHTML = alertIcon;
      document.getElementById('result-content').innerHTML
        = error.toString() + '\n'
        + '\nCheck your settings to make sure they are valid.';
    });
}

chrome.tabs.query({active: true, lastFocusedWindow: true}, tabs => {
  let url = tabs[0].url;
  document.getElementById('url').placeholder = url;
  getIngrs()
});
